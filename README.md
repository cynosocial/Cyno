#[Cyno](http://cyno.co/) - Social networking service
######"The beast of connections"



Stay up to date with the latest release, news and announcements on Twitter:
[@cynosocial](http://twitter.com/cynosocial).

Join us at https//cyno.co/

##License & Copyright
- Full details: http://cyno.co/index.php?a=page&b=about

##Changelog
- [v1.3.4-beta](https://github.com/cynosocial/Cyno/releases/tag/v1.3.4-beta) - Bugfixes and tweaked UI-design
- [v1.4.0-beta](https://github.com/cynosocial/Cyno/releases/tag/v1.4.0-beta) - New infrastructure and UI-design
- [v2.0.0-beta](https://github.com/cynosocial/Cyno/releases/tag/v2.0.0-beta) - **Coming 9th of August 2015**

##Contributing

Please read through our [contributing guidelines](https://github.com/cynosocial/Cyno/CONTRIBUTING.md).
Included are directions for opening issues, coding standards, and notes on development.

##Versioning

Cyno will be maintained under the Semantic Versioning guidelines as much as possible. Releases will be numbered
with the following format:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backward compatibility bumps the major (and resets the minor and patch)
* New additions, including new icons, without breaking backward compatibility bumps the minor (and resets the patch)
* Bug fixes and misc changes bumps the patch

For more information on SemVer, please visit http://semver.org.

##Contact

- Email: info@cyno.co
- Twitter: http://twitter.com/cynosocial
- Google+: http://plus.google.com/+cynosocial
- Cyno: https://cyno.co/
